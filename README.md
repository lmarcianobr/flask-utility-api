# flask-utility-api

## Requisitos:
* Python 3

## Instalação:
1. Clone do projeto:
```git clone https://lmarcianobr@bitbucket.org/lmarcianobr/flask-utility-api.git```

2. Caso utilizar o venv, criar o ambiente e ativá-lo: ```py -3 -m venv venv``` e ```venv\Scripts\activate.bat```

3. Instalar dependências:
```pip install -r requirements.txt```

4. Iniciar o servidor:
```flask run```

## Informações sobre a API:
Dois endpoints estão disponíveis na API:

### 1. Conversão de temperatura: ```POST /temperature```

Método para converter temperatura entre Celius, Farenheight e Kelvin. O corpo da requisição deve conter os seguintes parâmetros:

* **origem**: Unidade de temperatura de origem, podendo ser Celsius (`"C"`), Farenheight (`"F"`) ou Kelvin (`"K"`);

* **destino**: Unidade de temperatura de destino, podendo ser Celsius (`"C"`), Farenheight (`"F"`) ou Kelvin (`"K"`);

* **valor**: O valor de temperatura em unidades de origem, para que seja convertido para a unidade de destino. Pode vir em string (`"20.6"`) ou int/float (`20.6`).

#### Exemplo de requisição

Esta requisição irá converter 56,8°C em Farenheight:

```json
{
    "origem": "C",
    "destino": "F",
    "valor": 56.8
}
```

Na resposta, será recebido um valor com a chave denominada **convertido**, que contém o resultado da conversão ou um erro, explicitando se houve alguma falha.

#### Resposta para a requisição do exemplo anterior:
```json
{
    "convertido": 134.24
}
```

#### Exemplo de resposta em caso de erro:
```json
{
    "date": "2020-08-02 15:38:40",
    "message": [
        "Informar a unidade de origem",
        "Informar a unidade de destino",
        "Informar um valor válido"
    ]
}
```

### 2. Consulta de CEP: ```GET /consulta/cep/<cep>```

Método para consulta de informações de endereço de CEP utilizando o serviço ViaCEP. O parâmetro `<cep>` deve conter o CEP a ser consultado, podendo ser tanto com o traço (`85803-000`) ou sem (`85803000`). A resposta conterá a cidade, estado e rua do CEP correspondente caso este exista.

#### Resposta para a chamada ```GET /consulta/cep/85803000```
```json
{
    "cidade": "Cascavel",
    "estado": "PR",
    "rua": "Rua Carlos Gomes"
}
```

São feitas validações sobre o CEP rebebido antes de enviá-lo para o serviço ViaCEP, caso as validações falhem, uma mensagem de erro é enviada:

#### Resposta para a chamada ```GET /consulta/cep/8888```
```json
{
    "date": "2020-08-02 16:17:09",
    "message": "CEP inválido"
}
```

Caso o CEP seja válido porém inexistente, retornando erro do ViaCEP, tem-se o seguinte:

#### Resposta para a chamada ```GET /consulta/cep/91009009```
```json
{
    "date": "2020-08-02 16:17:56",
    "message": "CEP inexistente"
}
```

Existe uma memorização das requisições sucedidas. As 10 requisições mais utilizadas são guardadas na memória do servidor e utilizadas para não precisar realizar uma requisição ao ViaCEP.