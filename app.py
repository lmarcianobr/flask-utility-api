from flask import Flask
from exceptions.invalid_usage import InvalidUsage

app = Flask(__name__)

import controllers.temperature
import controllers.cep

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    return error.to_json(), 400