from app import app
from exceptions.invalid_usage import InvalidUsage

import re

def validate_cep(cep):
    errors = []

    # Retira os traços da string
    clean_cep = cep.replace("-", "")

    if len(clean_cep) != 8:
        raise InvalidUsage("CEP inválido")

    # Regex para existência de apenas 8 dígitos
    cep_regex_matches = re.fullmatch("(\d{8}){1}", clean_cep)

    if cep_regex_matches is None:
        raise InvalidUsage("Cep inválido")
    else:
        return clean_cep
