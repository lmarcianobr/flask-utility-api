from app import app
from exceptions.invalid_usage import InvalidUsage
from models.temperatures.celsius import Celsius
from models.temperatures.farenheight import Farenheight
from models.temperatures.kelvin import Kelvin

permitidos = {
    "C": Celsius(),
    "F": Farenheight(),
    "K": Kelvin()
}

def validate_temperature(data):
    errors = []

    # Chaves que vêm da request
    origem = data.get('origem')
    destino = data.get('destino')
    valor = data.get('valor')

    # Para a conversão do valor para float, visto que pode vir como int ou string
    valor_float = None
    
    # Verificação de existência das chaves
    if origem is None:
        errors.append("Informar a unidade de origem")

    if destino is None:
        errors.append("Informar a unidade de destino")

    if valor is None:
        errors.append("Informar o valor para conversão")

    # Verificação se o valor é um número válido
    try:
        valor_float = float(valor)
    except ValueError:
        errors.append("Informar um valor válido")

    # Se estiver tudo certo até aqui, as chaves existem
    # Então prosseguir com a validação das mesmas
    if len(errors) == 0:
        min_origem = permitidos.get(origem).lowest_allowed

        # Verifica se a origem se encontra na lista
        if permitidos.get(origem) is None:
            errors.append("Unidade de origem inválida")

        # Verifica se o destino se encontra na lista
        if permitidos.get(destino) is None:
            errors.append("Unidade de destino inválida")

        # Como o valor é para a origem, verifica se ele não é menor que o permitido
        if min_origem is not None and valor_float < min_origem:
            errors.append("Valor de temperatura inválido")

    # Se houver erros, lançar a exceção
    if (len(errors) > 0):
        raise InvalidUsage(errors)

    # Do contrário, passar o valor em float para o objeto e retorná-lo
    else:
        data['valor'] = valor_float
        return data
        