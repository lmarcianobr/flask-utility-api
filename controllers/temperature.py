from app import app
from flask import request

from models.temperatures.celsius import Celsius
from models.temperatures.farenheight import Farenheight
from models.temperatures.kelvin import Kelvin

from validators.temperature import validate_temperature

permitidos = {
    "C": Celsius(),
    "F": Farenheight(),
    "K": Kelvin()
}

@app.route('/temperature', methods=['POST'])
def convert():
    # Realiza a validação
    data = validate_temperature(request.get_json())

    # Obtém os valores da requisição
    origem = data.get('origem')
    destino = data.get('destino')
    valor = data.get('valor')

    # Obtém as instâncias das temperaturas para a conversão
    unidade_origem = permitidos.get(origem)
    unidade_destino = permitidos.get(destino)

    return {
        "convertido": unidade_origem.convert(unidade_destino.symbol, valor)
    }