from app import app
from flask import request
from validators.cep import validate_cep
from exceptions.invalid_usage import InvalidUsage

import requests

cache = []
max_cache_items = 10

@app.route('/consulta/cep/<cep>', methods=['GET'])
def consulta_cep(cep):
    # Valida o CEP e retira os traços, se houver
    validated_cep = validate_cep(cep)

    # Verificação do cache
    cached_data = fetch_from_cache(validated_cep)

    if cached_data is not None:
        return cached_data

    # Realiza a requisição para o ViaCEP caso não esteja no cache
    app.logger.info("Requisitando ViaCEP: " + validated_cep)
    response = requests.get("https://viacep.com.br/ws/" + validated_cep + "/json/")

    if response.status_code == 400:
        raise InvalidUsage("CEP inválido")

    # Dados da resposta
    response_data = response.json()

    if response_data.get("erro") == True:
        raise InvalidUsage("CEP inexistente")

    # Se está aqui, a requisição retornou dados válidos
    data = {
        "rua": response_data.get("logradouro"),
        "cidade": response_data.get("localidade"),
        "estado": response_data.get("uf")
    }

    add_to_cache(validated_cep, data)

    return data

# Retornar um item do cache
def fetch_from_cache(cep):
    app.logger.info("Pesquisando no cache: " + cep)
    for item in cache:
        if item["query"] == cep:
            item["hits"] += 1
            return item["data"]

    return None

# Adicionar um item ao cache
def add_to_cache(cep, data):
    app.logger.info("Adicionando ao cache: " + cep)

    if (len(cache) >= max_cache_items):
        app.logger.info("Tamanho máximo do cache atingido. Removendo item com menor número de acessos.")
        remove_least_hit()

    cache.append({
        "query": cep,
        "hits": 1,
        "data": data
    })

# Remover o item menos acessado do cache
def remove_least_hit():
    lowest_hits = float('inf')
    lowest_item = None

    for item in cache:
        if item["hits"] < lowest_hits:
            lowest_hits = item["hits"]
            lowest_item = item
    
    if lowest_item is not None:
        app.logger.info("Removendo do cache: " + lowest_item["query"])
        cache.remove(lowest_item)
