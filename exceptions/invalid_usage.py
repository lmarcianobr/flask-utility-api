from datetime import datetime

class InvalidUsage(Exception):

    def __init__(self, message):
        Exception.__init__(self)
        self.message = message

    def to_json(self):
        return {
            "message": self.message,
            "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
