from models.temperature import Temperature

class Celsius(Temperature):

    symbol = "C"

    def __init__(self):
        Temperature.__init__(self, -273.15)

    def convert(self, unit, value):
        if (unit == self.symbol):
            return value
        elif (unit == "F"):
            return (value * 1.8) + 32
        elif (unit == "K"):
            return value + 273.15