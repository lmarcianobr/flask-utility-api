from models.temperature import Temperature

class Kelvin(Temperature):

    symbol = "K"

    def __init__(self):
        Temperature.__init__(self, 0)

    def convert(self, unit, value):
        if (unit == self.symbol):
            return value
        elif (unit == "C"):
            return value - 273.15
        elif (unit == "F"):
            return (value * 1.8) - 459.67