from models.temperature import Temperature

class Farenheight(Temperature):

    symbol = "F"

    def __init__(self):
        Temperature.__init__(self, -459.67)

    def convert(self, unit, value):
        if (unit == self.symbol):
            return value
        elif (unit == "C"):
            return (value - 32) / 1.8
        elif (unit == "K"):
            return (value + 459.67) * 5 / 9